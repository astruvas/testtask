﻿import {Component, Vue} from "vue-property-decorator";

@Component({
  name: "Pagination",
  props: ["model", "total", "page", "aria-controls"]
})
export default class Pagination extends Vue {
}
