import Vue from 'vue';
import VueRouter, {RouteConfig} from 'vue-router';

Vue.use(VueRouter);

export const routes: RouteConfig[] = [
  // {
  //   path: '*',
  //   name: 'NotFound',
  //   component: () => import('@/pages/errors/NotFound.vue'),
  //   meta: {
  //     title: 'Page not found',
  //   },
  // },
  {
    path: '/',
    name: 'List',
    component: () => import('@/pages/list/List.vue'),
    meta: {
      title: 'List',
    },
  },
  {
    path: '/library/:id',
    name: 'Library',
    component: () => import('@/pages/library/Library.vue'),
    meta: {
      title: 'Library',
    },
  },
];

const createRouter = () => new VueRouter({
  mode: 'history',
  scrollBehavior: (to, from, savedPosition) => {
    if(savedPosition) {
      return savedPosition;
    } else {
      return {x: 0, y: 0};
    }
  },
  base: process.env.BASE_URL,
  routes: routes
});

const router = createRouter();

export default router;
