import {Route, RouteConfig} from 'vue-router';
import router, {routes} from '@/core/router';
import nProgress from 'nprogress';
import settings from '@/core/config/settings';
import { DataModule } from '@/core/store/modules/data';

import "nprogress/nprogress.css";

nProgress.configure({showSpinner: false});

const getPageTitle = (key: string) => {
  if(key) {
    return key + " - " + settings.title;
  }

  return settings.title;
}

router.beforeEach(async (to: Route, _: Route, next: any) => {
  nProgress.start();

  if(DataModule.data.id === 0) {
    await DataModule.GetData();
  }

  next();
  nProgress.done();
});

router.afterEach((to: Route) => {
  nProgress.done();
  document.title = getPageTitle(to.meta.title);
});


