import {RouteConfig} from 'vue-router';

export interface ISettings {
  title: string;
  routes: RouteConfig[];
}

const settings: ISettings = {
  title: "Libraries",
  routes: []
}

export default settings;
