export interface IRoot {
  data: IData;
}

export interface IData {
  id: number;
  name: string;
  description: string;
  status: string;
  address: IAddress;
}

export interface IAddress {
  street: string;
  comment: string;
  fullAddress: string;
}
