import Vue from 'vue';
import Vuex from 'vuex';
import {IRoot} from './interfaces/data';

Vue.use(Vuex);

export default new Vuex.Store<IRoot>({});
