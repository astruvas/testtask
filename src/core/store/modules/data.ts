import store from '@/core/store';
import { Action, Module, Mutation, getModule, VuexModule } from 'vuex-module-decorators';
import { getData } from '@/core/http/models/data';
import {IData, IRoot } from '../interfaces/data';

@Module({dynamic: true, store, name: 'data'})
class Data extends VuexModule implements IRoot {
  public data = {
    id: 0,
    name: '',
    description: '',
    status: '',
    address: {
      street: '',
      comment: '',
      fullAddress: '',
    },
    locale: {
      id: 0,
      sysName: ''
    }
  };
  public loading: boolean = true;

  @Action
  public async GetData() {
    await getData()
      .then((response) => {
        this.SET_DATA(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  @Mutation
  private async SET_DATA(model: any) {
    this.loading = true;
    this.data = model;
    this.loading = false;
  }

}

export const DataModule = getModule(Data);
