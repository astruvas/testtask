import axios from 'axios';

const http = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: process.env.VUE_APP_TIMEOUT,
});

http.interceptors.request.use((config) => {
    config.headers['Content-Type'] = 'application/json';

    return config;
  },
  (error) => {
    return Promise.reject(error);
  });

http.interceptors.response.use((response) => {
    if (response.status !== 200) {
      return Promise.reject(new Error(response.data.message || 'Unidentified error'));
    } else {
      return response;
    }
  },
  (error) => {
    const { status } = error.response;

    if (status === 500) {
      return Promise.reject(new Error(error || 'Unidentified error'));
    }
  });

export default http;
