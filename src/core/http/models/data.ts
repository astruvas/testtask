import http from '@/core/http';

export const getData = () =>
  http({
    url: '/',
    method: 'get',
  });
