import '@babel/polyfill';
import 'mutationobserver-shim';
import Vue from 'vue';
import '@/plugins/bootstrap-vue';
import '@/plugins/scrollbar';
import App from './App.vue';
import router from '@/core/router';
import store from '@/core/store';
import '@/core/config';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
