import { Component, Vue } from 'vue-property-decorator';
import { DataModule } from '@/core/store/modules/data';

@Component({
  name: 'List'
})
export default class List extends Vue {
  private collection: any = DataModule.data;
  private reset: boolean = true;

  async mounted() {
    await this.getList();
  }

  data() {
    return {
      perPage: 30,
      current: 1,
      rows: 0,
      sortBy: "id",
      sortDesc: false,
      fields: [
        {
          key: "id",
          label: "ID",
          sortable: true
        },
        {
          key: "name",
          label: "Name",
          sortable: true
        },
        {
          key: "locale.name",
          label: "Locale",
          sortable: true
        },
        {
          key: "category.name",
          label: "Category",
          sortable: true
        },
        {
          key: "status",
          label: "Status",
          sortable: true
        }
      ],
      list: []
    }
  }

  private async getList() {
    this.collection.map((item: any) => {
      this.$data.list.push(item.data.general);
    });

    this.$data.rows = this.$data.list.length;
  }

  private async localeFilter(e: any, data: number) {
    e.preventDefault();
    const array: any = [];
    this.collection.map((item: any) => {
      if(item.data.general.locale.id === data) {
        array.push(item.data.general);
      }
    });

    this.$data.list = array;
    this.$data.rows = this.$data.list.length;
    this.reset = false;
  }

  private async categoryFilter(e: any, data: string) {
    e.preventDefault();
    const array: any = [];
    this.collection.map((item: any) => {
      if(item.data.general.category.name === data) {
        array.push(item.data.general);
      }
    });

    this.$data.list = array;
    this.$data.rows = this.$data.list.length;
    this.reset = false;
  }

  private async resetFilter() {
    await this.getList();
  }
}
