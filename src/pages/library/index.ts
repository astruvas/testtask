import { DataModule } from '@/core/store/modules/data';
import {Component, Vue} from 'vue-property-decorator';

@Component({
  name: 'Library'
})
export default class Library extends Vue {
  private collection: any = DataModule.data;

  data() {
    return {
      item: {}
    }
  }

  private async mounted() {
    await this.getLibrary();
  }

  private async getLibrary() {
    const array: any = [];
    this.collection.map((item: any) => {
      array.push(item.data.general);
    });
    let filter = array.find((e: any) => e.id === Number(this.$route.params.id));

    this.$data.item = filter;
    console.log(filter);
    document.title = filter.name;
  }

  private async back() {
    this.$router.push('/');
  }
}
