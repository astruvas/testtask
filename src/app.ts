﻿import {Component, Vue} from "vue-property-decorator";

@Component({
  name: "App"
})
export default class App extends Vue {
  data() {
    return {
      perPage: 30,
      current: 1,
      rows: 0,
    }
  }
}
